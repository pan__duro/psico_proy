#define DEBUG
#define ROJO 13 //uso led bicolor y hago amarillo con rojo+verde
#define VERDE 2
#define START 4
#define STOP 5                                 //stop tiene le mismo efecto que si se cumplio el tiempo, termina el ensayo y muestra resultados
#define MAX_DATACHANS 8                        //cantidad maxima de canales
#define DATACHANS 4                            //de 1 a 8
#define TSAMPLE 100                            //milisegundos
#define TRESHLD 818                            //0 - 1024 (818 ~4V)
#define TEST_T (1 * 60)                        // tiempo del ensayo (en segundos)
#define EEPROM_DATA_LEN (3 * sizeof(uint16_t)) //cantidad de datos a guardar en eeprom

#include <Arduino.h>
#include <ctype.h>
#include <util/crc16.h>

uint16_t calcCRC(uint8_t *str, uint16_t len)
{
  uint16_t crc = 0;                 // starting value as you like, must be the same before each calculation
  for (uint8_t i = 0; i < len; i++) // for each character in the string
  {
    crc = _crc16_update(crc, str[i]); // update the crc value
  }
  return crc;
}

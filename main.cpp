#include <Arduino.h>
#include <EEPROM.h>
#include "defines.h"
#include "crc.h"
/*
TO-DO:
Procesamiento señales (filtro IIR para mediciones en modo datalogger / schmitt trigger en modo medicion) 
*/

typedef enum
{ //Variable para guardar el estado actual
  TEST,
  WAIT,
  WAIT_R,
  RU,
  ST
} status_t;

volatile status_t stat = WAIT;
volatile unsigned int datachans = DATACHANS, tsample = TSAMPLE, trshld = TRESHLD;


void setup()
{
  uint8_t i, eeprom_data[EEPROM_DATA_LEN];
  char str[80];
  uint16_t crc, stored_crc;
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode(START, INPUT_PULLUP);
  pinMode(STOP, INPUT_PULLUP);
  pinMode(ROJO, OUTPUT);
  pinMode(VERDE, OUTPUT);
  
  for (i = 0; i < EEPROM_DATA_LEN; i++)
  {
    eeprom_data[i] = EEPROM.read(i);
  }
  crc = calcCRC(eeprom_data, EEPROM_DATA_LEN);
  stored_crc = (EEPROM.read(EEPROM_DATA_LEN) << 8) + EEPROM.read(EEPROM_DATA_LEN + 1); //guardo en litle ending
  sprintf(str, "DATA   CRC: \t %X \nSTORED CRC: \t %X \n", crc, stored_crc);
  Serial.print(str);

  if (crc != stored_crc)
  {
    Serial.println("ERROR DE CRC, SE UTILIZAN VALORES POR DEFECTO");
  }
  else
  {
    Serial.println("CRC valido");
    datachans = (eeprom_data[0] << 8) + eeprom_data[1];
    tsample = (eeprom_data[2] << 8) + eeprom_data[3];
    trshld = (eeprom_data[4] << 8) + eeprom_data[5];
  }
  sprintf(str, "Canales: \t%u \nT. Sampleo: \t%u \nUmbral:\t\t%u", datachans, tsample, trshld);
  Serial.println(str);
}

void config(void)
{
  uint8_t i, eeprom_data[EEPROM_DATA_LEN];
  char str[5];
  uint16_t aux_u;
  int16_t aux;
  Serial.println("Rutina de configuración \n ingrese la cantidad de canales (1-8)\n");
  for (i = 0; i < 5; i++)
  {
    while (Serial.available() == 0)
    {
    } //espero que llege un caracter
    str[i] = Serial.read();
    Serial.print(str[i]);
    if (str[i] == '\n')
      break;
  }
  str[4] = '\0';   //termino el string por seguridad
  aux = atoi(str); //leo el valor
  if (0 < aux && aux < 9)
  { //verifico que este en el rango
    aux_u = aux;
    eeprom_data[0] = (aux_u >> 8);
    eeprom_data[1] = (aux_u & 0xFF);
    Serial.print("cantidad de canales seleccionada:");
    Serial.println(aux);
  }
  else
  {
    Serial.println("Error en el ingreso de datos");
    return;
  }

  Serial.println("ingrese el tiempo de sampleo en milisegundos (50-1000)\n");
  for (i = 0; i < 5; i++)
  {
    while (Serial.available() == 0)
    {
    } //espero que llege un caracter
    str[i] = Serial.read();
    Serial.print(str[i]);
    if (str[i] == '\n')
      break;
  }
  str[4] = '\0';              //termino el string por seguridad
  aux = atoi(str);            //leo el valor
  if (50 < aux && aux < 1000) //verifico que este en el rango
  {
    aux_u = aux;
    eeprom_data[2] = (aux_u >> 8);
    eeprom_data[3] = (aux_u & 0xFF);
    Serial.print("tiempo de sampleo seleccionado:");
    Serial.println(aux);
  }
  else
  {
    Serial.println("Error en el ingreso de datos");
    return;
  }
  /*   while (Serial.available() != 0)
  {
    Serial.read(); // vacío el buffer
  } */

  Serial.println("ingrese el valor de umbral (20-1000)\n");
  for (i = 0; i < 5; i++)
  {
    while (Serial.available() == 0)
    {
    } //espero que llege un caracter
    str[i] = Serial.read();
    Serial.print(str[i]);
    if (str[i] == '\n')
      break;
  }
  str[4] = '\0';              //termino el string por seguridad
  aux = atoi(str);            //leo el valor
  if (20 < aux && aux < 1000) //verifico que este en el rango
  {
    aux_u = aux;
    eeprom_data[4] = (aux_u >> 8);
    eeprom_data[5] = (aux_u & 0xFF);
    Serial.print("valor de umbral seleccionado:");
    Serial.println(aux);
  }
  else
  {
    Serial.println("Error en el ingreso de datos");
    return;
  }

  for (i = 0; i < EEPROM_DATA_LEN; i++) // si llegó hasta acá escribo los datos en la EEPROM
  {
    EEPROM.write(i, eeprom_data[i]);
  }
  aux_u = calcCRC(eeprom_data, EEPROM_DATA_LEN);
  EEPROM.write(EEPROM_DATA_LEN, aux_u >> 8);
  EEPROM.write(EEPROM_DATA_LEN + 1, aux_u & 0xFF);
}

void loop()
{
  if (Serial.available() > 0)
  {
    switch (Serial.read())
    {
    case 'i':
    case 'I':
      stat = WAIT_R;
      break;

    case 'f':
    case 'F':
      if (stat == RU) //lo paro solo si esta corriendo
      {
        stat = ST;
      }
      break;

    case 't':
    case 'T':
      stat = TEST;
      break;
    case 'c':
    case 'C':
      config();
      break;
    default:
      break;
    }
  }
  static unsigned char temp, i;
  static unsigned int timecounts[MAX_DATACHANS]; // cantiad maxima de canales
  static unsigned long start_t,last_t = 0; //Static preserva el valor entre llamadas de la funcion
  int aux;

  if (stat == TEST)
  { // en este estado espera que el usuario comience el ensayo
    if (millis() - last_t >= TSAMPLE)
    {
      last_t = millis();
      for (i = 0; i < datachans; i++)
      {
        Serial.print(" ");
        Serial.print(analogRead(i));
        Serial.print(",\t");
      }
      Serial.print("\n");
    }
    digitalWrite(VERDE, HIGH);
    if (digitalRead(START) == 0)
    {
      stat = WAIT_R;
      temp = 0;
      for (i = 0; i < datachans; i++)
      {
        timecounts[i] = 0; //inicializo los contadores a cero
      }
      digitalWrite(VERDE, LOW);
    }
  }
  if (stat == WAIT)
  { // en este estado espera que el usuario comience el ensayo
    digitalWrite(VERDE, HIGH);
    if (digitalRead(START) == 0)
    {
      stat = WAIT_R;
      temp = 0;
      for (i = 0; i < datachans; i++)
      {
        timecounts[i] = 0; //inicializo los contadores a cero
      }
      digitalWrite(VERDE, LOW);
    }
  }
  if (stat == WAIT_R)
  { // en este estado espera que alguna rata de inicio a las mediciones
    if ((millis() / 250) % 2 == 1)
    {
      digitalWrite(ROJO, HIGH);
      digitalWrite(VERDE, HIGH);
    }
    else
    {
      digitalWrite(ROJO, LOW);
      digitalWrite(VERDE, LOW);
    }
    for (i = 0; i < datachans; i++)
    {
      temp += (analogRead(i) >= TRESHLD);
    }
    if (temp != 0)
    {
      stat = RU;
      start_t = millis();
    }
  }
  if (stat == RU)
  { // este estado realiza la medicion
    static unsigned long last_t = 0;
    digitalWrite(ROJO, HIGH);
    digitalWrite(VERDE, HIGH);
    if (millis() - last_t >= TSAMPLE)
    {
      last_t = millis();
      for (i = 0; i < datachans; i++)
      {
        if (analogRead(i) >= TRESHLD)
        {
          timecounts[i]++;
        }
      }
      if (((millis() - start_t) / 1000) >= TEST_T || digitalRead(STOP) == 0)
      {
        stat = ST;
      }
    }
  }
  if (stat == ST)
  { // Presenta los datos en una terminal serie
    digitalWrite(ROJO, HIGH);
    digitalWrite(VERDE, LOW);
    aux = ((millis() - start_t) / 1000);
    if (aux <= TEST_T)
    {
      Serial.print("TRABAJO DETENIDO POR EL USUARIO \n Tiempo total:");
      Serial.println(aux);
    }
    Serial.print(" ---- Resultados ---- \n tiempo en segundos para los sensores 1 a 4 respectivamente:\n\n");
    for (i = 0; i < datachans; i++)
    {
      Serial.print(" ");
      Serial.print((timecounts[i] * TSAMPLE) / 1000.0);
      Serial.print("\t");
    }
    Serial.print("\n");
    stat = WAIT;
  }
}
